import express  from 'express';
import util from 'utility'
import jwt from 'jsonwebtoken';  


const router = express.Router();  



router.get('/list',async (req,res)=>{
    const data = await req.prisma.Brand.findMany({
        where:{
            lv:3
        }
    })

    res.success(data);
})

export default  router;

