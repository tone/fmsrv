import express  from 'express';
import { checkSchema, validationResult } from 'express-validator'  
import util from 'utility'
import multer from 'multer';
import axios from 'axios';
const router = express.Router();  


// 配置 Multer  
const storage = multer.diskStorage({  
    destination: function (req, file, cb) {  
      cb(null, 'public/uploads/') // 上传文件的保存路径  
    },  
    filename: function (req, file, cb) {  
        if(file.originalname.startsWith('upload')){
             cb(null,file.originalname)
        } else {
            const ext = file.originalname.split(".").pop();

            cb(null, Date.now()+'.'+ext) // 文件名，默认为 file.originalname  
        }
   
    }  
  });  
const upload = multer({ storage: storage });  
    

router.get('/getPos',(req,res)=>{
    
})

router.post('/upload', upload.array('file'), (req, res) => { 
    console.log(req.files) 
    // 处理上传成功的逻辑  
    res.success('ok');  
  });  
    

router.post('/sendCode',checkSchema({
    phone: { notEmpty: true, errorMessage:'手机号码不能为空' }
}),(req,res)=>{
    res.validator();
    const { type = 'login', phone } = req.body;
    const saveCode = req.nodeCache.get(`code_${type}_${phone}`);
    if(saveCode) {
        res.fail('已经发送_'+saveCode)
    }else {
        const rnd = util.randomString(4,'1234567890');
        req.nodeCache.set(`code_${type}_${phone}`, rnd, 300);
        res.success({code: rnd})
    }
})

router.post('/driverPlan', async(req,res)=>{
        const {origin,destination } = req.body;
        const teamarr = await axios.get('https://restapi.amap.com/v5/direction/driving',{
            params:{
                key:'b66f8dc02cb315c691792bd9cf161676',
                origin,
                destination,
                strategy:1,
                show_fields:'tmcs,navi'
            }
        });

        res.send(teamarr.data)
    
})


export default router

